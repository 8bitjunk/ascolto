@extends('layouts.app')

@section('css')
    <link href="/css/welcome.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="content">
            <div class="row">
                <div class="col-md-6  hidden-sm hidden-xs " id="welcome-background"></div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="nav navbar-nav navbar-right" id="welcome-navbar">
                                <!-- Authentication Links -->
                                @if (Auth::guest())
                                    <li><a href="{{ url('/login') }}">Login</a></li>
                                    <li><a href="{{ url('/register') }}">Register</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{ url('/logout') }}"
                                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <img class="centre-logo" src="/img/logo.svg" alt="logo created by Paola Oliani, taken from The Noun Project">
                            <div class="title m-b-md">
                                {{ Config::get('app.name') }}
                            </div>

                            <div class="links">
                                <a href="{{ url('/#') }}">Features</a>
                                <a href="{{ url('/#') }}">Pricing</a>
                                <a href="{{ url('/#') }}">About</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection