@extends('layouts.app')

@section('css')
    <link href="/css/allBooks.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="heading">Deleted Books</h3>
                    <p>Click a book to restore it to your library.  If you run low on storage space, we'll remove these first, starting with the oldest.</p>
                </div>
            </div>
            <div class="row">
                <div id="book-list col-md-12">
                    @if($books->isEmpty())
                        <div class="content-empty">
                            <i class="fa fa-bookmark-o" id="book-icon"></i>
                            <h4>There are no deleted books</h4>
                            <br>
                            <a href="{{ route('books.index') }}" class="btn btn-default"><i class="fa fa-bookmark"></i> My Library</a>
                        </div>
                    @else
                        @foreach($books as $book)
                            @include('layouts.bookTile')
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
@endsection