@extends('layouts.app')

@section('css')
    <link href="/css/uploadBook.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="container">
        <div class="content">
            @include('layouts.uploadForm')
        </div>
    </div>
@endsection

@section('script')

@endsection