@extends('layouts.app')

@section('css')
    <link href="/css/allBooks.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="heading">Library</h3>
                    <p>This is your library, where all your uploaded books live.  Click a book to begin playback.</p>
                </div>
            </div>
            <div class="row">
                <div id="book-list">
                    @if($books->isEmpty())
                        <div class="content-empty">
                            <i class="fa fa-bookmark-o" id="book-icon"></i>
                            <h4>There are no books</h4>
                            <br>
                            <a href="{{ route('books.create') }}" class="btn btn-default"><i class="fa fa-upload"></i> Upload New</a>
                        </div>
                    @else
                        @foreach($books as $book)
                            @include('layouts.bookTile')
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection