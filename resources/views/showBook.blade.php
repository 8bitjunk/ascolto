@extends('layouts.app')

@section('css')
    <link href="/css/showBook.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('books.index') }}">Library</a></li>
                        <li class="active">{{ $book->title }}</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div style="background-image: url({{ $book->coverUrl }});" class="book-cover"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 player">
                    <audio src="{{ $book->streamUrl }}" preload="auto" controls id="player" class="pull-left"></audio>
                    <div class="pull-right buttons">
                            <a href="{{ route('books.edit', ['id' => $book->id]) }}"><i class="fa fa-pencil"></i> Edit</a>

                            <a href="{{ route('books.destroy', ['id' => $book->id])}}"
                               onclick="
                                       event.preventDefault();
                                       if(confirm('Delete {{ $book->title }}?')) {
                                       player.pause();
                                       document.getElementById('delete-form-{{ $book->id }}').submit();
                                       }">
                                <i class="fa fa-trash"></i> Delete
                            </a>

                            <form id="delete-form-{{ $book->id }}" action="{{ route('books.destroy', ['id' => $book->id]) }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var player = document.getElementById('player');

        window.onload = function() {
            player.currentTime = {{ $book->position }};
        };

        window.onbeforeunload = function() {
            player.pause();

            $.ajax({
                url: '{{ route('books.savePosition', ['book' => $book->id]) }}',
                type: 'POST',
                data: {
                    position: player.currentTime,
                    _token: "{{ csrf_token() }}"
                }
            });
        };
    </script>
@endsection