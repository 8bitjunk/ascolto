<div class="book-container col-md-2">
    @if($book->trashed())
        <form id="restore-form-{{ $book->id }}" action="{{ route('books.restore', ['id' => $book->id]) }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

        <a href="{{ route('books.restore', $book->id) }} " onclick="
            event.preventDefault();
            if(confirm('Restore {{ $book->title }}?')) {
            document.getElementById('restore-form-{{ $book->id }}').submit();
        }">
            <div style="background-image: url({{ $book->coverUrl }});" class="book-cover"></div>
        </a>
    @else
        <a href="{{ route('books.show', $book->id) }}">
            <div style="background-image: url({{ $book->coverUrl }});" class="book-cover"></div>
        </a>
    @endif
    <div class="book-info">
        <div class="book-title">
            <strong>{{ $book->title }}</strong>,
        </div>
        <div class="book-author">
            {{ $book->author }}
        </div>
    </div>
</div>