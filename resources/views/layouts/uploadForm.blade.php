<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Upload New Audio File</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('books.store') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title" class="col-md-4 control-label">Title</label>

                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" autofocus>

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
                        <label for="author" class="col-md-4 control-label">Author</label>

                        <div class="col-md-6">
                            <input id="author" type="text" class="form-control" name="author" value="{{ old('author') }}">

                            @if ($errors->has('author'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('author') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('fileUpload') ? ' has-error' : '' }}">
                        <label for="fileUpload" class="col-md-4 control-label">Upload File</label>

                        <div class="col-md-6">
                            <input type="file" name="fileUpload" id="fileUpload" required>
                            <p class="help-block">Currently only MP3 files are supported.</p>

                            @if ($errors->has('fileUpload'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('fileUpload') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('imageUpload') ? ' has-error' : '' }}">
                        <label for="imageUpload" class="col-md-4 control-label">Upload Cover Image</label>

                        <div class="col-md-6">
                            <input type="file" name="imageUpload" id="imageUpload" required>
                            <p class="help-block">This file will be displayed as artwork for the audio.</p>

                            @if ($errors->has('imageUpload'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('imageUpload') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-upload"></i> Upload
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>