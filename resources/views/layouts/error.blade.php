@extends('layouts.app')

@section('css')
    <link href="/css/error.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="container">
        <div class="content col-sm-12">
            <div class="title">{{ $exception->getStatusCode() }}</div>
            @yield('error-info')
            <br />
            <a href={{ route('welcome') }}>Home</a>
        </div>
    </div>
@endsection