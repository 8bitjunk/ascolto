@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="/css/editBook.css">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 upload-form">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-xs-6">Upload New Audio File</div>
                                <div class="col-xs-6">
                                    <a href="{{ route('books.index') }}" class="pull-right" id="cancel-button">
                                        <i class="fa fa-chevron-left"></i> Cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('books.update', ['id' => $book->id]) }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" value="{{ $book->title }}" autofocus>

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
                                <label for="author" class="col-md-4 control-label">Author</label>

                                <div class="col-md-6">
                                    <input id="author" type="text" class="form-control" name="author" value="{{ $book->author }}">

                                    @if ($errors->has('author'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('author') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('imageUpload') ? ' has-error' : '' }}">
                                <label for="imageUpload" class="col-md-4 control-label">Change Cover Image</label>

                                <div class="col-md-6">
                                    <input type="file" name="imageUpload" id="imageUpload">
                                    <p class="help-block">This file will be displayed as artwork for the audio.</p>

                                    @if ($errors->has('imageUpload'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('imageUpload') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4 col-md-offset-5">
                                <div style="background-image: url({{ $book->coverUrl }});" class="book-cover"></div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-floppy-o"></i> Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection