@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="/css/home.css">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome, {{ Auth::user()->name }}</div>
                <div class="panel-body">
                    <h5 id="chart-heading">Storage Quota</h5>
                    <canvas id="quota-chart"></canvas>
                    <div class="donut-inner">
                        <h5>{{ $usedSpace['total'] }} / {{ $totalSpace }}</h5>
                        <span>(GB)</span>
                    </div>
                    <div class="col-sm-12" id="welcome-buttons">
                        <a href="{{ route('books.index') }}" class="btn btn-default"><i class="fa fa-bookmark"></i> My Library</a>
                        <a href="{{ route('books.create') }}" class="btn btn-default"><i class="fa fa-upload"></i> Upload New</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.js"></script>
    <script>
        var ctx = $("#quota-chart");

        var data = {
            labels: [
                "Library",
                "Deleted",
                "Free"
            ],
            datasets: [
                {
                    data: [{{ $usedSpace['used'] }}, {{ $usedSpace['deleted'] }}, 10],
                    backgroundColor: [
                        "#484e51",
                        "#d3dcde",
                        "#f0f3f4"
                    ],
                    hoverBackgroundColor: [
                        "#484e51",
                        "#d3dcde",
                        "#f0f3f4"
                    ],
                    borderColor: [
                        '#484e51',
                        '#d3dcde',
                        '#f0f3f4'
                    ],
                    borderWidth: 0.1,
                    borderCapStyle: 'round'
                }]
        };

        var options = {
            legend: {
                position: 'bottom',
                display: true
            },
            tooltips: {
                enabled: true
            }
        };

        var chart = new Chart(ctx, {
            type: 'doughnut',
            data: data,
            options: options
        });
    </script>
@endsection
