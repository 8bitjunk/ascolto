@extends('layouts.error')

@section('error-info')
    <div class="subtitle">{{ $exception->getMessage() ? $exception->getMessage() : 'service unavailable' }}</div>
    <div>Just tidying up. Back soon.</div>
    <div>Sorry about that.</div>
@endsection