@extends('layouts.error')

@section('error-info')
    <div class="subtitle">{{ $exception->getMessage() ? $exception->getMessage() : 'not found' }}</div>
    <div>The page you were looking for was not found.</div>
    <div>Sorry about that.</div>
@endsection