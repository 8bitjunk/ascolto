@extends('layouts.error')

@section('error-info')
    <div class="subtitle">{{ $exception->getMessage() ? $exception->getMessage() : 'internal error' }}</div>
    <div>It seems we're having some problems.</div>
    <div>Sorry about that.</div>
@endsection