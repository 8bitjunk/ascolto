@extends('layouts.error')

@section('error-info')
    <div class="subtitle">{{ $exception->getMessage() ? $exception->getMessage() : 'forbidden' }}</div>
    <div>You are not authorised to access this resource.</div>
    <div>Sorry about that.</div>
@endsection