<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    if(Auth::user()){
        return redirect(route('home'));
    } else {
        return view('welcome');
    }
})->name('welcome');

Route::get('/info', function() {
   phpinfo();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/books/deleted', 'BookController@showDeleted')->name('books.showDeleted');

Route::resource('books', 'BookController');

Route::post('/books/{book}/restore', 'BookController@restore')->name('books.restore');

Route::resource('users', 'UserController');

