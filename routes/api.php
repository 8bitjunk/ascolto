<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

// route to handle book file streaming, uses middleware for resource access authorisation
Route::get('/files/{book}', 'BookController@stream')->name('books.stream');

//route to handle book image retrieval
Route::get('/files/{book}/cover', 'BookController@cover')->name('books.cover');

Route::post('/books/{book}/position', 'BookController@savePosition')->name('books.savePosition');