<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalSpace = round(((((Auth::user()->quota) / 1024) / 1024) / 1024), 1); // to GB, round UP

        $usedSpace = [];
        foreach(Auth::user()->usedQuota as $key => $value) {
            $usedSpace[$key] = round(((($value / 1024) / 1024) / 1024), 1);
        }

        return view('home', compact('usedSpace', 'totalSpace'));
    }
}
