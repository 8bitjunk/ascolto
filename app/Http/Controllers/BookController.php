<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Book;

class BookController extends Controller
{

    // set middleware for auth, make sure user is logged in and is accessing a book uploaded by them
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['']]);
        $this->middleware('resource.auth', ['except' => ['index', 'create', 'store', 'showDeleted']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Auth::user()->books;
        return view('allBooks', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('uploadBook');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate post parameters
        $this->validate($request, [
            'title' => 'bail|required|string',
            'author' => 'bail|required|string',
            'fileUpload' => 'bail|required',
            'imageUpload' => 'bail|required|mimes:jpeg,bmp,png',
        ]);

        // create, store and return the newly created book
        $book = Book::create([
            'title' => $request->input('title'),
            'author' => $request->input('author'),
            'file_path' => "files/" . Auth::user()->id . "/books/",
            'file_extension' => $request->file('fileUpload')->getClientOriginalExtension(),
            'image_path' => "files/" . Auth::user()->id . "/images/",
            'image_extension' => $request->file('imageUpload')->getClientOriginalExtension(),
            'uploaded_by' => Auth::user()->id,
            'file_size' => $request->file('fileUpload')->getClientSize(),
        ]);

        // store book & image files to disk

        Storage::putFileAs($book->file_path, $request->file('fileUpload'), $book->id . "." . $book->file_extension);
        Storage::putFileAs($book->image_path, $request->file('imageUpload'), $book->id . "." . $book->image_extension);

        return redirect()->action('BookController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Book $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view('showBook', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Book $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view('editBook', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Book $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $this->validate($request, [
            'title' => 'bail|required|string',
            'author' => 'bail|required|string',
            'imageUpload' => 'bail|mimes:jpeg,bmp,png',
        ]);

        // set updated fields
        $book->title = $request->input('title');
        $book->author = $request->input('author');

        // if the user uploads a replacement image, overwrite the previous one
        if($request->file('imageUpload')) {
            Storage::delete($book->coverPath);
            Storage::putFileAs($book->image_path, $request->file('imageUpload'), $book->id . "." . $book->image_extension);
        }

        // save field changes and redirect to show page
        $book->save();
        return redirect()->action('BookController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Book $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        if(!$book->trashed()) {
            $book->delete();
        }

        return redirect()->action('BookController@index');
    }

    public function showDeleted() {
        $books = Auth::user()->books()->onlyTrashed()->get();
        return view('showDeleted', compact('books'));
    }

    public function restore($id) {
        $book = Book::withTrashed()->whereId($id)->get()->first();
        if($book->trashed()) {
            $book->restore();
        }
        return redirect()->action('BookController@showDeleted');
    }

    public function stream(Book $book)
    {
        // return stored file for book to stream in view
        return response()->file(storage_path($book->streamingPath));
    }

    public function cover(Book $book) {
        return response()->file(storage_path($book->coverPath));
    }

    public function savePosition(Request $request, Book $book) {
        $book->position = floor($request->input('position'));
        return $book->save() ? Response::HTTP_OK : Response::HTTP_INTERNAL_SERVER_ERROR;
    }
}
