<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Book;

class CheckResourceAccessAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if the book we are trying to access was uploaded by the currently logged in user
        // allow, otherwise abort with 403 FORBIDDEN

        $book = is_object($request->book)
            ? $request->book
            : Book::withTrashed()->whereId($request->book)->get()->first();

        return $book->user == Auth::user() ? $next($request) : abort(403, "forbidden");
    }
}
