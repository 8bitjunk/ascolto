<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
  use SoftDeletes;

  protected $dates = ['deleted_at'];

  protected $fillable = [
      'title',
      'author',
      'file_path',
      'image_path',
      'position',
      'uploaded_by',
      'file_extension',
      'image_extension',
      'file_size',
  ];

  public function user()
  {
      return $this->belongsTo('App\User', 'uploaded_by');
  }

  public function getStreamUrlAttribute()
  {
      return route('books.stream', $this->id);
  }

  public function getStreamingPathAttribute() {
      return "app/{$this->file_path}/{$this->id}.{$this->file_extension}";
  }

  public function getCoverUrlAttribute() {
      return route('books.cover', $this->id);
  }

  public function getCoverPathAttribute() {
      return "app/{$this->image_path}/{$this->id}.{$this->image_extension}";
  }
}
