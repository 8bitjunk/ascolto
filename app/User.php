<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function books()
    {
        return $this->hasMany('App\Book', 'uploaded_by');
    }

    /**
     * Get the user's used storage space.
     * Used storage is split into Used, Deleted and Total
     * in an associative array.
     *
     * @return array
     */
    public function getUsedQuotaAttribute() {
        $used = 0;
        $deleted = 0;

        foreach($this->books as $book) {
            $used += $book->file_size;
        }

        foreach($this->books()->onlyTrashed()->get() as $book) {
            $deleted += $book->file_size;
        }

        return ['used' => $used, 'deleted' => $deleted, 'total' => ($used + $deleted)];
    }

    public function getQuotaAttribute() {
        return 1024 * 1024 * 1024 * 10; // 10GB
    }
}
